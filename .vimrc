set nocompatible              " required
filetype off                  " required
set nu
set mouse=a
set nowrap
set smartcase

" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()

" alternatively, pass a path where Vundle should install plugins
"call vundle#begin('~/some/path/here')

" let Vundle manage Vundle, required
Plugin 'gmarik/Vundle.vim'

" Add all your plugins here (note older versions of Vundle used Bundle instead of Plugin)

" General Vim Plugins
Plugin 'scrooloose/syntastic'
Plugin 'scrooloose/nerdtree'
"Plugin 'jistr/vim-nerdtree-tabs'
Plugin 'bling/vim-airline'
Plugin 'wesQ3/vim-windowswap'
Plugin 'kien/ctrlp.vim'
Plugin 'ervandew/supertab'

" Python Specific
Plugin 'vim-scripts/indentpython.vim'
Plugin 'nvie/vim-flake8'
"Plugin 'davidhalter/jedi-vim'

" Appearance
Plugin 'godlygeek/csapprox'
Plugin 'jnurmine/Zenburn'
Plugin 'morhetz/gruvbox'
Bundle 'flazz/vim-colorschemes'
Bundle 'altercation/vim-colors-solarized'
Bundle 'ColorSchemeMenuMaker'
Bundle 'desert-warm-256'

"Bundle 'Valloric/YouCompleteMe'

" All of your Plugins must be added before the following line
call vundle#end()            " required

colorscheme gruvbox
set background=dark

filetype plugin indent on    " required

au BufNewFile,BufRead *.py
    \ set tabstop=4 |
    \ set softtabstop=4 |
    \ set shiftwidth=4 |
    \ set textwidth=79 |
    \ set expandtab |
    \ set autoindent |
    \ set fileformat=unix |

au BufNewFile,BufRead *.js,*.html,*.css
    \ set tabstop=2 |
    \ set softtabstop=2 |
    \ set shiftwidth=2 |

au BufNewFile,BufRead *.hs
    \ set smartcase |
    \ set smarttab |
    \ set smartindent |
    \ set autoindent |
    \ set softtabstop=2 |
    \ set shiftwidth=2 |
    \ set expandtab |
    \ set incsearch |
    \ set clipboard=unnamedplus,autoselect |
    \ set completeopt=menuone,menu,longest |
    \ set wildignore+=*\\tmp*,*.swp,*.swo,*.zip,.git,.cabal-sandbox |
    \ set wildmode=longest,list,full |
    \ set wildmenu |
    \ set completeopt+=longest |
    \ set cmdheight=1

let NERDTreeIgnore=['\.pyc$', '\~$', '\.o$'] "ignore files in NERDTree
nnoremap <F5> :NERDTreeToggle<CR>

let g:airline_powerline_fonts=1

let python_highlight_all=1
syntax on
set laststatus=2

let mapleader=" "

"python with virtualenv support
python3 << EOF
import os
import sys
if 'VIRTUAL_ENV' in os.environ:
	project_base_dir = os.environ['VIRTUAL_ENV']
	activate_this = os.path.join(project_base_dir, 'bin/activate_this.py')
	#execfile(activate_this, dict(__file__=activate_this)) # python2
	exec(compile(open(activate_this, "rb").read(), activate_this, 'exec'), dict(__file__=activate_this))
EOF

set t_Co=256
