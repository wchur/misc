set nocompatible              " required
filetype off                  " required
set nu
set relativenumber
set mouse=a
"set nowrap
set smartcase

call plug#begin('~/.config/nvim/plugged')

" General Vim Plugins
Plug 'scrooloose/syntastic'
Plug 'scrooloose/nerdtree'
Plug 'bling/vim-airline'
Plug 'wesQ3/vim-windowswap'
Plug 'kien/ctrlp.vim'
Plug 'ervandew/supertab'

" Python
Plug 'vim-scripts/indentpython.vim'
Plug 'nvie/vim-flake8'
"Plugin 'davidhalter/jedi-vim'
Plug 'Shougo/deoplete.nvim', {'do': ':UpdateRemotePlugins'}
Plug 'zchee/deoplete-jedi'
Plug 'zchee/deoplete-clang'

let g:python_host_prog = '/home/will/Envs/neovim-python2/bin/python'
let g:python3_host_prog = '/home/will/Envs/neovim/bin/python'

" Haskell
Plug 'neovimhaskell/haskell-vim'

" Golang
Plug 'fatih/vim-go'

" Appearance
Plug 'jnurmine/Zenburn'
Plug 'morhetz/gruvbox'
Plug 'https://github.com/freeo/vim-kalisi'
"Plug 'altercation/vim-colors-solarized'
Plug 'frankier/neovim-colors-solarized-truecolor-only'
"Plug 'desert-warm-256'
Plug 'mhartington/oceanic-next'
Plug 'nanotech/jellybeans.vim'
Plug 'zcodes/vim-colors-basic'
Plug 'rhysd/vim-color-spring-night'
Plug 'KeitaNakamura/neodark.vim'

call plug#end()

colorscheme gruvbox
set background=dark

filetype plugin indent on    " required

au BufNewFile,BufRead *.py
    \ set tabstop=4 |
    \ set softtabstop=4 |
    \ set shiftwidth=4 |
"    \ set textwidth=79 |
    \ set expandtab |
    \ set autoindent |
    \ set fileformat=unix |

au BufNewFile,BufRead *.js,*.go,
    \ set tabstop=4 |
    \ set softtabstop=4 |
    \ set shiftwidth=4 |

au BufNewFile,BufRead *.html,*.css,*.yml
    \ set tabstop=2 |
    \ set softtabstop=2 |
    \ set shiftwidth=2 |

au BufNewFile,BufRead *.hs
    \ set smartcase |
    \ set smarttab |
    \ set smartindent |
    \ set autoindent |
    \ set tabstop=8 |
    \ set expandtab |
    \ set softtabstop=4 |
    \ set shiftwidth=4 |
    \ set shiftround |
    \ set incsearch |
    \ set clipboard=unnamedplus,autoselect |
    \ set completeopt=menuone,menu,longest |
    \ set wildignore+=*\\tmp*,*.swp,*.swo,*.zip,.git,.cabal-sandbox |
    \ set wildmode=longest,list,full |
    \ set wildmenu |
    \ set completeopt+=longest |
    \ set cmdheight=1

let NERDTreeIgnore=['\.pyc$', '\~$', '\.o$'] "ignore files in NERDTree
nnoremap <F5> :NERDTreeToggle<CR>

let g:airline_powerline_fonts=1

let python_highlight_all=1
syntax on
set laststatus=2

let mapleader=" "

set backspace=indent,eol,start

nmap <leader>l :set list!<CR>
nmap <leader>t :w<bar>GoTest<CR>
nmap <leader>i :GoImports<CR>
nmap io i<CR><ESC>O
inoremap jj <ESC>

let g:deoplete#enable_at_startup = 1
let g:deoplete#sources#clang#libclang_path = '/usr/lib/llvm-4.0/lib/libclang.so'
let g:deoplete#sources#clang#clang_header = '/usr/lib/llvm-4.0/lib/clang'
let g:ctrlp_cmd='CtrlP :pwd'

set termguicolors
let NVIM_TUI_ENABLE_TRUE_COLOR=1
